# BBootstrap

## A framework for expediting front-end development with Drupal + Bootstrap.

This theme is designed with that philosophy to expedite developing themes with bootstrap. It provides an easy on-ramp for designing an MVP site quickly, and being able to maintain the site design afterward.

My philosophy of theme management:  UI should be controlled globally from a single source of truth, but also allow for customization of unique components when desired, kind of like Bootstrap...

The theme's settings provide control over Bootstrap component defaults, while components may override these defaults in markup- the companion module makes it easy to accomplish this with Views style plugins for many components.

## Getting Started

Install and enable the BBootstrap theme.

Get your custom project started by creating a subtheme using Drupal's Admin UI (`/admin/appearance/settings/bbootstrap`). From the "Subtheme" menu, name and describe your theme, then click "Create".

Good work, you've created a Drupal theme!

## Build Your Theme

Now you should be ready to customize your theme (*if not, check permissions*)... Enable your subtheme and set as default (*Admin UI or drush it*).

BBootstrap utilizes an npm based workflow to run tasks, providing maximum flexibility and inclusion of any module available in the npm universe.

Open a terminal and:
```
cd path/to/drupal/themes/yourtheme
npm install
```
This will install all devDependencies... Postinstall it will copy the current bootstrap/bootswatch/popper to your theme's `/src/lib` and run an initial build so you know things are working right.

The following commands are provided out of the box:
```
npm run

    - dev // browsersync watch:*
    - prod // clean build lint:fixcss
    - build // build:*
    - watch // watch:* lint:js
    - lint:css
    - lint:js
    - lint:fixcss
    - lint:sass
    - build:css
    - build:js
    - build:assets
    - watch:sass
    - watch:js
    - watch:images
    - browsersync
    - clean:assets
    - clean:css
    - clean:js
    - clean // rimraf dist
    - copy
    - postcss
    - sass
    - js:beautify
    - js:uglify
    - icons
    - imagemin
    - favicon
```

## Styling

Styling is primarily managed by modifying the variables in `/src/sass/require/bbootstrap/`(*as recommended by bootstrap*).

The bootswatch theme library is available through theme settings- I recommend finding a theme style you mostly like... then you can simply edit edit the theme locally and choose it in the theme settings.

 & google fonts can be changed in the Drupal Admin theme settings, which allows for quickly finding a theme that works for you without needing too much modification.

To simplify maintenance bootstrap's SASS variables are separated out into partials. Variable values are the default bootstrap values, but will override when sass is compiled.

## Javascript

Bootstrap.js default component settings are made available in Drupal's Admin UI (Appearance>Theme>Settings) and passed to the frontend with `drupalSettings.bbootstrap`.

Custom javascript is provided in the theme library and available in `$YOURTHEME/src/js/$YOURTHEME.js/`.

## Templates

It is recommended that you use layout builder for your custom layouts- a sample layout is setup in your subthemes `/layouts` directory, but others may be added by modifying `yourtheme.layouts.yml` + creating `yourtemplate.twig.html`.

If you must, you may create a `region--$REGION_NAME.html.twig` file for custom content in specific regions.

## Additional Notes
    - Preprocess functions are used to pass config settings to templates.
        - More accessible to designers who want to simply add a class through Drupal's Admin UI.
        - Allows import/export of config so settings can be shared and managed through code.
        - Bootstrap variables organized in a more maintainable order.
